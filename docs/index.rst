.. Nursery Tracker documentation master file, created by
   sphinx-quickstart on Wed Jul  8 12:59:30 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Nursery Tracker Documentation
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

The documentation, maintained with the help of the community, offers instructions on how to install, configure, and use Nursery Tracker®.
Whether you are new to Nursery Tracker, or a seasoned user, our docs offer something for everyone.

Index
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* :ref:`myob`
